// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.RunCommand;
import frc.robot.commands.OperateDrive;
import frc.robot.subsystems.DriveUtil;


/**
 * This class is where the bulk of the robot should be declared. Since Command-based is a
 * "declarative" paradigm, very little robot logic should actually be handled in the {@link Robot}
 * periodic methods (other than the scheduler calls). Instead, the structure of the robot (including
 * subsystems, commands, and button mappings) should be declared here.
 */
public class RobotContainer {
  // Subsystems
  private final DriveUtil driveUtil = new DriveUtil();
  
  // Commands
  private final OperateDrive operateDrive = new OperateDrive(driveUtil);
  // Controllers
  private static Joystick leftJoystick, rightJoystick;
  // JoystickButton objects
  // private JoystickButton $button0, $button1;

  /** The container for the robot. Contains subsystems, OI devices, and commands. */
  public RobotContainer() {
    leftJoystick = new Joystick(Constants.leftJoystick);
    rightJoystick = new Joystick(Constants.rightJoystick);
    // Configure the button bindings
    configureButtonBindings();
    // Configure the default commands
    configureSubsystemDefaultCommands();
  }


  
  /**
   * Use this method to define your button->command mappings. Buttons can be created by
   * instantiating a {@link GenericHID} or one of its subclasses ({@link
   * edu.wpi.first.wpilibj.Joystick} or {@link XboxController}), and then passing it to a {@link
   * edu.wpi.first.wpilibj2.command.button.JoystickButton}.
   */
  private void configureButtonBindings() {
    // Initialize JoystickButton objects
    // $commandName = new JoystickButton($Controller, Button.k$ButtonName.value);
    // Define what each JoystickButton does
    // $commandName.whenPressed(new InstantCommand(() -> $subSystem.commandToBeRun(), $subSystem)); OR $commandName.whileHeld(new $commandToBeRun($subSystem));
  }

  private void configureSubsystemDefaultCommands() {
    driveUtil.setDefaultCommand(new RunCommand(() -> driveUtil.driveRobot(), driveUtil));
    
  } 

  /*
   * Controller get() methods - returns numerical value of axis
   */
  // Driver controller
  
  
  

  public static double getLeftJoystickX() {
    return leftJoystick.getX();
  }

  public static double getLeftJoystickY() {
    return leftJoystick.getY();
  }

  public static double getRightJoystickX() {
    return rightJoystick.getX();
  }

  public static double getRightJoystickY() {
    return rightJoystick.getY();
  }

  
  

  
  
  /**
   * Use this to pass the autonomous command to the main {@link Robot} class.
   *
   * @return the command to run in autonomous
   */
  public Command getAutonomousCommand() {
    // An ExampleCommand will run in autonomous
    return null;
  }
}
