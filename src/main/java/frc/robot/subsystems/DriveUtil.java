// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;
import com.ctre.phoenix.motorcontrol.can.WPI_VictorSPX;
import com.kauailabs.navx.frc.AHRS;

import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.SPI;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import frc.robot.RobotContainer;

public class DriveUtil extends SubsystemBase {
  // NavX MXP Stuff
  // AHRS navX;
  
  // Motor controllers
  private WPI_VictorSPX leftPrimary , rightPrimary;
  private WPI_TalonSRX  leftSecondary,  rightSecondary;
  private Encoder leftEncoder, rightEncoder;

  // Speed variables
  private double speed, rotation, gyroAngle, navXAngle;

  // Drive controller
  private DifferentialDrive differentialDrive;

  
  /** Creates a new DriveUtil. */
  public DriveUtil() {

    // Initialize speed controllers (TalonSRX)
    leftPrimary = new WPI_VictorSPX(Constants.driveMotor_LeftPrimary);
    leftSecondary = new WPI_TalonSRX(Constants.driveMotor_LeftSecondary);
    rightPrimary = new WPI_VictorSPX(Constants.driveMotor_RightPrimary);
    rightSecondary = new WPI_TalonSRX(Constants.driveMotor_RightSecondary);

    // Initialize encoders (Quad Encoders using DIO)
    leftEncoder = new Encoder(Constants.leftEncoder_channelA, Constants.leftEncoder_channelB);
    rightEncoder = new Encoder(Constants.rightEncoder_channelA, Constants.rightEncoder_channelB);

    // Initialize NavX MXP
    // navX = new AHRS(SPI.Port.kMXP);

    // navX.calibrate();
    // navX.reset();

    leftPrimary.setNeutralMode(NeutralMode.Brake);
    rightPrimary.setNeutralMode(NeutralMode.Brake);
    leftSecondary.setNeutralMode(NeutralMode.Brake);
    rightSecondary.setNeutralMode(NeutralMode.Brake);

    // Set secondaries to follow primaries
    leftSecondary.follow(leftPrimary);
    rightSecondary.follow(rightPrimary);

    // Invert motors
    leftPrimary.setInverted(true);
    leftSecondary.setInverted(true);
    // rightPrimary.setInverted(true);
    // rightSecondary.setInverted(true);

    // Initialize DifferentialDrive controller
    differentialDrive = new DifferentialDrive(leftPrimary, rightPrimary);
  }

  

  public void driveRobot() {
    double leftY, rightY;
    leftY = RobotContainer.getLeftJoystickY();
    rightY = RobotContainer.getRightJoystickY();
    differentialDrive.tankDrive(leftY, rightY);
  }

  public double getGyroAngle() {
    // navXAngle = navX.getAngle();
    // if (navXAngle <= 0) {
    //   gyroAngle = navXAngle + 360;
    // } else {
    //   gyroAngle = navXAngle;
    // }
    // return gyroAngle;
    return 0;
  }


  @Override
  public void periodic() {
    // This method will be called once per scheduler run
    
  }
}

